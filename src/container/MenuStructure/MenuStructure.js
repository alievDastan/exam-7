import React, {Component} from 'react';
import Dish from '../../component/Dish/Dish'
import AssemblyOfControls from '../../component/Dish/AssemblyOfControls/AssemblyOfControls'
import Modal from '../../component/UI/Modal/Modal'
import './MenuStructure.css'

class MenuStructure extends Component {
    constructor(props) {
        super(props);

        this.state = {
            structure: [
                {name: 'hamburger', label: 'Гамбургер', count: 0, price: 80, disable: true},
                {name: 'cheeseburger', label: 'Чизбургер', count: 0, price: 90, disable: true},
                {name: 'fries', label: 'Картофель фри', count: 0, price: 45, disable: true},
                {name: 'coffee', label: 'Кофе', count: 0, price: 70, disable: true},
                {name: 'tea', label: 'Чай', count: 0, price: 50, disable: true},
                {name: 'cola', label: 'Кока-кола', count: 0, price: 40, disable: true},
            ],
            totalPrice: 0,
            isModalOpen: false
        }

        this.addItems = (type) => {
            const updatedIngredients = this.state.structure.map(ingredient => {
                if (ingredient.name === type) {
                    ingredient.count++;
                    ingredient.disable = false;
                    // eslint-disable-next-line react/no-direct-mutation-state
                    this.state.totalPrice = this.state.totalPrice + ingredient.price;
                }
                return ingredient;
            });

            this.setState({
                structure: [].concat(updatedIngredients),
            })
        }

        this.removeItems = (type) => {
            const updatedIngredients = this.state.structure.map(ingredient => {
                if (ingredient.name === type) {
                    if (ingredient.count > 0) {
                        ingredient.count--;
                        ingredient.disable = false;
                        // eslint-disable-next-line react/no-direct-mutation-state
                        this.state.totalPrice = this.state.totalPrice - ingredient.price;
                    }
                    if (ingredient.count === 0) {
                        ingredient.disable = true;
                    }
                }
                return ingredient;
            });

            this.setState({
                structure: [].concat(updatedIngredients),
            })
        }

        this.openModal = () => {
            this.setState(
                {isModalOpen: true}
            )
        }

        this.closeModal = () => {
            this.setState(
                {isModalOpen: false}
            )
        }

        this.continuePurchase = () => {
            alert('Ваш заказ будет скоро готов!');
            this.setState(
                {
                    structure: [
                        {name: 'hamburger', label: 'Гамбургер', count: 0, price: 80, disable: true},
                        {name: 'cheeseburger', label: 'Чизбургер', count: 0, price: 90, disable: true},
                        {name: 'fries', label: 'Картофель фри', count: 0, price: 45, disable: true},
                        {name: 'coffee', label: 'Кофе', count: 0, price: 70, disable: true},
                        {name: 'tea', label: 'Чай', count: 0, price: 50, disable: true},
                        {name: 'cola', label: 'Кока-кола', count: 0, price: 40, disable: true},
                    ],
                    totalPrice: 0,
                    isModalOpen: false
                })
        }
    }

    render() {
        return (
            <div className="allBlock">
                {this.state.isModalOpen ?
                    <Modal
                        structure={this.state.structure}
                        isModalOpen={this.state.isModalOpen}
                        closeModal={this.closeModal}
                        continuePurchase={this.continuePurchase}
                        totalPrice={this.state.totalPrice}/> : null
                }
                <Dish structure={this.state.structure}/>
                <AssemblyOfControls
                    removeItems={this.removeItems}
                    addItems={this.addItems}
                    totalPrice={this.state.totalPrice}
                    structure={this.state.structure}
                    openModal={this.openModal}/>
            </div>
        );
    }
}

export default MenuStructure;
