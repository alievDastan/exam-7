import './App.css';
import Content from "./component/Content/Content";
import MenuStructure from "./container/MenuStructure/MenuStructure";
import '@material-ui/core';

const App = () => {
    return (
        <div className="App">
            <Content>
                <MenuStructure/>
            </Content>
        </div>
    );
}

export default App;
