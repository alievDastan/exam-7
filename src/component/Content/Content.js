import React from 'react';

const Content = (props) => {
    return (
        <div className='Content'>
            <main>{props.children}</main>
        </div>
    );
}

export default Content;
