import React from 'react';
import './Dish.css'
import MenuComposition from './MenuComposition/MenuComposition'

const dish = (props) => {

    let updatedIngredients = [];
    let ifEmpty = 'Вы ничего не выбрали';

    props.structure.map(newConstruction);

    function newConstruction(ingredient) {
        for (let i = 1; i <= ingredient.count; i++) {
            updatedIngredients.push(
                <MenuComposition type={ingredient.name}/>
            )
        }
    }

    return (
        <div className='Dish'>
            <div className="foodBlock">
                {updatedIngredients.length ? updatedIngredients : ifEmpty}
            </div>
        </div>
    );
}

export default dish;
