import React from 'react';
import './AssemblyOfControl.css';
import {Button} from "@material-ui/core";

const AssemblyOfControl =(props)=> {

    return (
        <div className='AssemblyOfControl'>
            <div className='Label'>{props.label}</div>
            <p className='count'>Кол-во: {props.count}</p>
            <Button variant="contained" color="primary" disabled={props.disable} onClick={props.removeItems}>-</Button>
            <Button variant="contained" color="primary" onClick={props.addItems}>+</Button>
            <p>Цена: {props.price} сом</p>
        </div>
    )
}

export default AssemblyOfControl;
