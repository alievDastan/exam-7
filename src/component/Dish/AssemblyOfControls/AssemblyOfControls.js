import React from 'react';
import AssemblyOfControl from './AssemblyOfControl/AssemblyOfControl'
import './AssemblyOfControls.css'
import { Button} from "@material-ui/core";

const assemblyOfControls = (props) => {

    return (
        <div className='assemblyOfControls'>
            <p className='price'>Сумма: {props.totalPrice + ' сом'}</p>
            {props.structure.map(ctrl =>
                <AssemblyOfControl
                    key={ctrl.name}
                    label={ctrl.label}
                    count={ctrl.count}
                    price={ctrl.price}
                    addItems={() => props.addItems(ctrl.name)}
                    removeItems={() => props.removeItems(ctrl.name)}
                    disable={ctrl.disable}
                />
            )}
            <Button className="orderBtn" variant="contained" color="secondary" disabled={props.totalPrice === 0} onClick={props.openModal}>
                Заказать
            </Button>
        </div>
    );
}

export default assemblyOfControls;
