import React, {Component} from 'react';
import './MenuComposition.css'

class MenuComposition extends Component {
    render() {
        let ingredient = null;

        switch (this.props.type) {

            case('hamburger'):
                ingredient = <div className='Hamburger'>
                    <img src="https://freepngimg.com/thumb/burger%20sandwich/12-hamburger-burger-png-image.png" alt=""/>
                    <p className="foodTitle">hamburger</p>
                </div>;
                break;

            case('cheeseburger'):
                ingredient = <div className='Cheeseburger'>
                    <img src="https://mcdonalds.co.nz/sites/mcdonalds.co.nz/files/Triple%20Cheeseburger%20700x487.png" alt=""/>
                    <p className="foodTitle">Чизбургер</p>
                </div>;
                break;

            case('fries'):
                ingredient = <div className='Fries'>
                    <img src="https://lh3.googleusercontent.com/proxy/A7DOJMxuIrNO4eXTi6iHbtZR1GoS5AMAx26EvD37FjudZ_vtQsRkDLwotdyooea6FThh05QYRZRYAZAw7HKEuRe502au7YWKQ4Ni7q-nAzkweurUsQe1gHU7SktwS42wzmPgP7wOdTQIrV-gdHvr2I1Be1ZgcP0HrO5PAv8uIr7VJg" alt=""/>
                    <p className="foodTitle">Картофель фри</p>
                </div>;
                break;

            case('coffee'):
                ingredient = <div className='Coffee'>
                    <img src="https://www.softnhost.com/resto/10-2-coffee-png-pic-thumb.png" alt=""/>
                    <p className="foodTitle">Кофе</p>
                </div>;
                break;

            case('tea'):
                ingredient = <div className='Tea'>
                    <img src="https://freepngimg.com/thumb/tea/5-2-tea-png-image-thumb.png" alt=""/>
                    <p className="foodTitle">Чай</p>
                </div>;
                break;

            case('cola'):
                ingredient = <div className='Cola'>
                    <img src="https://www.freepngimg.com/thumb/coca%20cola/5-coca-cola-bottle-png-image-thumb.png" alt=""/>
                    <p className="foodTitle">Coca-cola</p>
                </div>;
                break;

            default:
                ingredient = null;
        }
        return ingredient;
    }
}

export default MenuComposition;

