import React from 'react';
import './Background.css'

const background = (props) => {

    return (
        <div>
            {props.isModalOpen ? <div className='background' onClick={props.closeModal}></div> : null}
        </div>
    );
}

export default background;
