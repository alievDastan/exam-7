import React from 'react';
import './Modal.css';
import Background from '../Background/Background'

const modal = (props) => {
    return (
        <div>
            <Background isModalOpen={props.isModalOpen} closeModal={props.closeModal}/>

            <div className='Modal'>
                <h6 className='heading'>Ваш заказ:</h6>
                <ul className='list'>
                    {props.structure.map(ingredient =>
                        <li>{ingredient.label}: {ingredient.count}</li>
                    )}
                </ul>
                <p className='heading'>Сумма заказа: {props.totalPrice + ' сом'}</p>
                <p className='list'>Подтвердить данный заказ?</p>
                <button className="btn btn-success btn-sm" onClick={props.continuePurchase}> Подтвердить </button>
                &emsp;

                <button className="btn btn-danger btn-sm" onClick={props.closeModal}> Отменить</button>

            </div>

        </div>

    );

}

export default modal
